const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  try {
    const query = `
          SELECT
            mtc.id AS config_id,
            mtc.name AS config_name,
            mtc.display_order,
            mtr.rank,
            mtr.manager_id,
            m.name AS manager_name,
            mtr.gold,
            mtr.silver,
            mtr.bronze
          FROM medal_table_configs mtc
          LEFT JOIN medal_table_rows mtr ON mtc.id = mtr.config_id
          LEFT JOIN managers m ON mtr.manager_id = m.id
          ORDER BY mtc.display_order, mtr.rank
        `;

    const result = await pool.query(query);
    const medalTables = [];

    result.rows.forEach((row) => {
      const configId = row.config_id;
      const configName = row.config_name;
      const medalTable = medalTables.find(
        (table) => table.configId === configId
      );

      if (!medalTable) {
        medalTables.push({
          configId,
          configName,
          rows: [],
        });
      }

      medalTables
        .find((table) => table.configId === configId)
        .rows.push({
          rank: row.rank,
          managerId: row.manager_id,
          managerName: row.manager_name,
          gold: row.gold,
          silver: row.silver,
          bronze: row.bronze,
        });
    });

    res.json(medalTables);
  } catch (error) {
    console.error("Error retrieving medal tables:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/export", async (req, res) => {
  try {
    const query = `
          SELECT
            mtc.name AS config_name,
            mtr.rank,
            m.name AS manager_name,
            mtr.gold,
            mtr.silver,
            mtr.bronze
          FROM medal_table_configs mtc
          LEFT JOIN medal_table_rows mtr ON mtc.id = mtr.config_id
          LEFT JOIN managers m ON mtr.manager_id = m.id
          ORDER BY mtc.display_order, mtr.rank
        `;

    const result = await pool.query(query);
    let exportText = "";

    const calculateTabs = (content, desiredWidth) => {
      const contentLength = content.length;
      const tabsNeeded = Math.max(
        0,
        Math.ceil((desiredWidth - contentLength) / 4)
      );
      return "\t".repeat(tabsNeeded);
    };

    let currentConfigName = "";
    result.rows.forEach((row) => {
      if (row.config_name !== currentConfigName) {
        currentConfigName = row.config_name;
        exportText += `\n\n${currentConfigName}\n`;
        exportText += "Rank\tManager\t\t\t\t\tGold\tSilver\tBronze\n";
      }
      if (row.rank) {
        const rankTabs = calculateTabs(row.rank.toString(), 8);
        const managerTabs = calculateTabs(row.manager_name, 24);
        const goldTabs = calculateTabs(row.gold.toString(), 8);
        const silverTabs = calculateTabs(row.silver.toString(), 8);
        exportText += `${row.rank}${rankTabs}${row.manager_name}${managerTabs}${row.gold}${goldTabs}${row.silver}${silverTabs}${row.bronze}\n`;
      } else {
        exportText += "\n";
      }
    });

    // Append result logs to the exported text
    exportText += "\n\nComputed Result Logs\n";
    const resultLogsQuery = `
      SELECT
        r.league_id,
        l.name AS league_name,
        l.start_year,
        l.end_year,
        r.gold_managers,
        r.silver_managers,
        r.bronze_managers,
        ARRAY(
          SELECT m.name
          FROM unnest(r.gold_managers) AS gm
          JOIN managers m ON m.id = gm
        ) AS gold_manager_names,
        ARRAY(
          SELECT m.name
          FROM unnest(r.silver_managers) AS sm
          JOIN managers m ON m.id = sm
        ) AS silver_manager_names,
        ARRAY(
          SELECT m.name
          FROM unnest(r.bronze_managers) AS bm
          JOIN managers m ON m.id = bm
        ) AS bronze_manager_names
      FROM results r
      JOIN leagues l ON r.league_id = l.id
      ORDER BY l.start_year, l.id
    `;
    const resultLogsResult = await pool.query(resultLogsQuery);
    resultLogsResult.rows.forEach((result) => {
      exportText += `${result.league_name} (${
        result.start_year === result.end_year
          ? result.start_year
          : `${result.start_year}-${result.end_year}`
      }): G - ${result.gold_manager_names.join(
        ", "
      )}, S - ${result.silver_manager_names.join(
        ", "
      )}, B - ${result.bronze_manager_names.join(", ")}\n`;
    });

    // Append individual award result logs to the exported text
    exportText += "\n\nIndividual Awards\n";
    const individualAwardResultLogsQuery = `
      SELECT
        iar.individual_award_id,
        ia.name AS individual_award_name,
        iar.start_year,
        iar.end_year,
        iar.manager_id,
        m.name AS manager_name,
        COUNT(*) OVER (
          PARTITION BY iar.individual_award_id, iar.manager_id
          ORDER BY iar.start_year, iar.id
        ) AS award_count
      FROM individual_award_results iar
      JOIN individual_awards ia ON iar.individual_award_id = ia.id
      JOIN managers m ON iar.manager_id = m.id
      ORDER BY iar.start_year, iar.id
    `;
    const individualAwardResultLogsResult = await pool.query(
      individualAwardResultLogsQuery
    );
    individualAwardResultLogsResult.rows.forEach((result) => {
      exportText += `${result.individual_award_name} (${
        result.start_year === result.end_year
          ? result.start_year
          : `${result.start_year}-${result.end_year}`
      }): ${result.manager_name} (${result.award_count})\n`;
    });

    res.setHeader("Content-Type", "text/plain");
    res.setHeader(
      "Content-Disposition",
      "attachment; filename=medal-tables.txt"
    );
    res.send(exportText);
  } catch (error) {
    console.error("Error exporting medal tables:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
