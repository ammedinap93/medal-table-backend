const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  const { individualAwardId, managerId, startYear, endYear } = req.query;
  try {
    let query = `
           SELECT iar.*, ia.name AS individual_award_name, m.name AS manager_name
           FROM individual_award_results iar
           JOIN individual_awards ia ON iar.individual_award_id = ia.id
           JOIN managers m ON iar.manager_id = m.id
         `;
    const values = [];
    const filters = [];

    if (individualAwardId) {
      filters.push("iar.individual_award_id = $" + (values.length + 1));
      values.push(individualAwardId);
    }
    if (managerId) {
      filters.push("iar.manager_id = $" + (values.length + 1));
      values.push(managerId);
    }
    if (startYear) {
      filters.push("iar.start_year = $" + (values.length + 1));
      values.push(startYear);
    }
    if (endYear) {
      filters.push("iar.end_year = $" + (values.length + 1));
      values.push(endYear);
    }

    if (filters.length > 0) {
      query += " WHERE " + filters.join(" AND ");
    }

    const result = await pool.query(query, values);
    res.json(result.rows);
  } catch (error) {
    console.error("Error retrieving individual award results:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/", async (req, res) => {
  const { individualAwardId, managerId, startYear, endYear } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO individual_award_results (individual_award_id, manager_id, start_year, end_year) VALUES ($1, $2, $3, $4) RETURNING *",
      [individualAwardId, managerId, startYear, endYear]
    );
    res.status(201).json(result.rows[0]);
  } catch (error) {
    console.error("Error adding individual award result:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { individualAwardId, managerId, startYear, endYear } = req.body;
  try {
    const result = await pool.query(
      "UPDATE individual_award_results SET individual_award_id = $1, manager_id = $2, start_year = $3, end_year = $4 WHERE id = $5 RETURNING *",
      [individualAwardId, managerId, startYear, endYear, id]
    );
    if (result.rows.length === 0) {
      res.status(404).json({ error: "Individual award result not found" });
    } else {
      res.json(result.rows[0]);
    }
  } catch (error) {
    console.error("Error updating individual award result:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await pool.query("DELETE FROM individual_award_results WHERE id = $1", [
      id,
    ]);
    res.sendStatus(204);
  } catch (error) {
    console.error("Error deleting individual award result:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
