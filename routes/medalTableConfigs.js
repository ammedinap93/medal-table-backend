const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  const { name, sportId, tier } = req.query;
  try {
    let query =
      "SELECT mtc.*, s.name AS sport_name FROM medal_table_configs mtc LEFT JOIN sports s ON mtc.sport_id = s.id";
    const values = [];
    const filters = [];

    if (name) {
      filters.push("mtc.name ILIKE $" + (values.length + 1));
      values.push(`%${name}%`);
    }
    if (sportId) {
      filters.push("mtc.sport_id = $" + (values.length + 1));
      values.push(sportId);
    }
    if (tier) {
      filters.push("mtc.tier = $" + (values.length + 1));
      values.push(tier);
    }

    if (filters.length > 0) {
      query += " WHERE " + filters.join(" AND ");
    }

    const result = await pool.query(query, values);
    res.json(result.rows);
  } catch (error) {
    console.error("Error retrieving medal table configs:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/", async (req, res) => {
  const { name, sportId, tier, displayOrder } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO medal_table_configs (name, sport_id, tier, display_order) VALUES ($1, $2, $3, $4) RETURNING *",
      [name, sportId || null, tier || null, displayOrder || 0]
    );
    res.status(201).json(result.rows[0]);
  } catch (error) {
    console.error("Error adding medal table config:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { name, sportId, tier, displayOrder } = req.body;
  try {
    const result = await pool.query(
      "UPDATE medal_table_configs SET name = $1, sport_id = $2, tier = $3, display_order = $4 WHERE id = $5 RETURNING *",
      [name, sportId || null, tier || null, displayOrder || 0, id]
    );
    if (result.rows.length === 0) {
      res.status(404).json({ error: "Medal table config not found" });
    } else {
      res.json(result.rows[0]);
    }
  } catch (error) {
    console.error("Error updating medal table config:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await pool.query("DELETE FROM medal_table_configs WHERE id = $1", [id]);
    res.sendStatus(204);
  } catch (error) {
    console.error("Error deleting medal table config:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/:id/recalculate", async (req, res) => {
  const { id } = req.params;
  try {
    // Delete existing medal table rows for the config
    await pool.query("DELETE FROM medal_table_rows WHERE config_id = $1", [id]);

    // Retrieve the medal table config
    const configResult = await pool.query(
      "SELECT * FROM medal_table_configs WHERE id = $1",
      [id]
    );
    const config = configResult.rows[0];

    // Retrieve the relevant results based on the config constraints
    let query = `
        SELECT
          r.id,
          r.league_id,
          r.gold_managers,
          r.silver_managers,
          r.bronze_managers
        FROM results r
        JOIN leagues l ON r.league_id = l.id
        JOIN competitions c ON l.competition_id = c.id
        JOIN sports s ON c.sport_id = s.id
        WHERE 1 = 1
      `;
    const values = [];

    if (config.sport_id) {
      query += " AND s.id = $" + (values.length + 1);
      values.push(config.sport_id);
    }
    if (config.tier) {
      query += " AND l.tier = $" + (values.length + 1);
      values.push(config.tier);
    }

    const resultsResult = await pool.query(query, values);
    const results = resultsResult.rows;

    // Process the results and create medal table rows
    const medalTableRows = [];
    const managerMedals = {};

    results.forEach((result) => {
      const { gold_managers, silver_managers, bronze_managers } = result;

      gold_managers.forEach((managerId) => {
        if (!managerMedals[managerId]) {
          managerMedals[managerId] = { gold: 0, silver: 0, bronze: 0 };
        }
        managerMedals[managerId].gold++;
      });

      silver_managers.forEach((managerId) => {
        if (!managerMedals[managerId]) {
          managerMedals[managerId] = { gold: 0, silver: 0, bronze: 0 };
        }
        managerMedals[managerId].silver++;
      });

      bronze_managers.forEach((managerId) => {
        if (!managerMedals[managerId]) {
          managerMedals[managerId] = { gold: 0, silver: 0, bronze: 0 };
        }
        managerMedals[managerId].bronze++;
      });
    });

    Object.entries(managerMedals).forEach(([managerId, medals]) => {
      medalTableRows.push({
        config_id: config.id,
        manager_id: parseInt(managerId),
        gold: medals.gold,
        silver: medals.silver,
        bronze: medals.bronze,
      });
    });

    // Calculate the rank for each manager
    medalTableRows.sort((a, b) => {
      if (a.gold !== b.gold) {
        return b.gold - a.gold;
      } else if (a.silver !== b.silver) {
        return b.silver - a.silver;
      } else {
        return b.bronze - a.bronze;
      }
    });

    let rank = 1;
    let prevGold = null;
    let prevSilver = null;
    let prevBronze = null;

    medalTableRows.forEach((row, index) => {
      if (
        row.gold !== prevGold ||
        row.silver !== prevSilver ||
        row.bronze !== prevBronze
      ) {
        rank = index + 1;
      }
      row.rank = rank;
      prevGold = row.gold;
      prevSilver = row.silver;
      prevBronze = row.bronze;
    });

    // Insert the medal table rows into the database
    const insertQueries = medalTableRows.map((row) => {
      const { config_id, manager_id, rank, gold, silver, bronze } = row;
      return pool.query(
        "INSERT INTO medal_table_rows (config_id, manager_id, rank, gold, silver, bronze) VALUES ($1, $2, $3, $4, $5, $6)",
        [config_id, manager_id, rank, gold, silver, bronze]
      );
    });

    await Promise.all(insertQueries);

    res.sendStatus(200);
  } catch (error) {
    console.error("Error recalculating medal table:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
