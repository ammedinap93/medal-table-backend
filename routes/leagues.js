const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  const { name, competitionId, sportId, tier, startYear, endYear } = req.query;
  try {
    let query =
      "SELECT l.*, c.name AS competition_name, s.name AS sport_name FROM leagues l JOIN competitions c ON l.competition_id = c.id JOIN sports s ON c.sport_id = s.id";
    const values = [];
    const filters = [];

    if (name) {
      filters.push("l.name ILIKE $" + (values.length + 1));
      values.push(`%${name}%`);
    }
    if (competitionId) {
      filters.push("l.competition_id = $" + (values.length + 1));
      values.push(competitionId);
    }
    if (sportId) {
      filters.push("s.id = $" + (values.length + 1));
      values.push(sportId);
    }
    if (tier) {
      filters.push("l.tier = $" + (values.length + 1));
      values.push(tier);
    }
    if (startYear) {
      filters.push("l.start_year = $" + (values.length + 1));
      values.push(startYear);
    }
    if (endYear) {
      filters.push("l.end_year = $" + (values.length + 1));
      values.push(endYear);
    }

    if (filters.length > 0) {
      query += " WHERE " + filters.join(" AND ");
    }

    const result = await pool.query(query, values);
    res.json(result.rows);
  } catch (error) {
    console.error("Error retrieving leagues:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/", async (req, res) => {
  const { name, competitionId, tier, startYear, endYear } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO leagues (name, competition_id, tier, start_year, end_year) VALUES ($1, $2, $3, $4, $5) RETURNING *",
      [name, competitionId, tier, startYear, endYear]
    );
    res.status(201).json(result.rows[0]);
  } catch (error) {
    console.error("Error adding league:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { name, competitionId, tier, startYear, endYear } = req.body;
  try {
    const result = await pool.query(
      "UPDATE leagues SET name = $1, competition_id = $2, tier = $3, start_year = $4, end_year = $5 WHERE id = $6 RETURNING *",
      [name, competitionId, tier, startYear, endYear, id]
    );
    if (result.rows.length === 0) {
      res.status(404).json({ error: "League not found" });
    } else {
      res.json(result.rows[0]);
    }
  } catch (error) {
    console.error("Error updating league:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await pool.query("DELETE FROM leagues WHERE id = $1", [id]);
    res.sendStatus(204);
  } catch (error) {
    console.error("Error deleting league:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
