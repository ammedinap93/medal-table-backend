const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  const { name, sportId } = req.query;
  try {
    let query =
      "SELECT c.*, s.name AS sport_name FROM competitions c JOIN sports s ON c.sport_id = s.id";
    const values = [];
    if (name) {
      query += " WHERE c.name ILIKE $1";
      values.push(`%${name}%`);
    }
    if (sportId) {
      query += values.length > 0 ? " AND" : " WHERE";
      query += " c.sport_id = $" + (values.length + 1);
      values.push(sportId);
    }
    const result = await pool.query(query, values);
    res.json(result.rows);
  } catch (error) {
    console.error("Error retrieving competitions:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/", async (req, res) => {
  const { name, sportId } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO competitions (name, sport_id) VALUES ($1, $2) RETURNING *",
      [name, sportId]
    );
    res.status(201).json(result.rows[0]);
  } catch (error) {
    console.error("Error adding competition:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { name, sportId } = req.body;
  try {
    const result = await pool.query(
      "UPDATE competitions SET name = $1, sport_id = $2 WHERE id = $3 RETURNING *",
      [name, sportId, id]
    );
    if (result.rows.length === 0) {
      res.status(404).json({ error: "Competition not found" });
    } else {
      res.json(result.rows[0]);
    }
  } catch (error) {
    console.error("Error updating competition:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await pool.query("DELETE FROM competitions WHERE id = $1", [id]);
    res.sendStatus(204);
  } catch (error) {
    console.error("Error deleting competition:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
