const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  try {
    const query = `
      SELECT
        r.id,
        r.league_id,
        l.name AS league_name,
        l.start_year,
        l.end_year,
        r.gold_managers,
        r.silver_managers,
        r.bronze_managers,
        ARRAY(
          SELECT m.name
          FROM unnest(r.gold_managers) AS gm
          JOIN managers m ON m.id = gm
        ) AS gold_manager_names,
        ARRAY(
          SELECT m.name
          FROM unnest(r.silver_managers) AS sm
          JOIN managers m ON m.id = sm
        ) AS silver_manager_names,
        ARRAY(
          SELECT m.name
          FROM unnest(r.bronze_managers) AS bm
          JOIN managers m ON m.id = bm
        ) AS bronze_manager_names
      FROM results r
      JOIN leagues l ON r.league_id = l.id
      ORDER BY l.start_year, l.id
    `;

    const result = await pool.query(query);
    res.json(result.rows);
  } catch (error) {
    console.error("Error retrieving result logs:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
