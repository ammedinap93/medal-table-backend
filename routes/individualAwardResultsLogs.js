const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  try {
    const query = `
      SELECT
        iar.id,
        iar.individual_award_id,
        ia.name AS individual_award_name,
        iar.start_year,
        iar.end_year,
        iar.manager_id,
        m.name AS manager_name,
        COUNT(*) OVER (
          PARTITION BY iar.individual_award_id, iar.manager_id
          ORDER BY iar.start_year, iar.id
        ) AS award_count
      FROM individual_award_results iar
      JOIN individual_awards ia ON iar.individual_award_id = ia.id
      JOIN managers m ON iar.manager_id = m.id
      ORDER BY iar.start_year, iar.id
    `;

    const result = await pool.query(query);
    res.json(result.rows);
  } catch (error) {
    console.error("Error retrieving individual award result logs:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
