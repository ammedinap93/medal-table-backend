const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  const {
    leagueName,
    sportId,
    competitionId,
    tier,
    startYear,
    endYear,
    managerId,
  } = req.query;
  try {
    let query = `
          SELECT
            r.id,
            r.league_id,
            l.name AS league_name,
            l.start_year,
            l.end_year,
            r.gold_managers,
            r.silver_managers,
            r.bronze_managers,
            ARRAY(
              SELECT m.name
              FROM unnest(r.gold_managers) AS gm
              JOIN managers m ON m.id = gm
            ) AS gold_manager_names,
            ARRAY(
              SELECT m.name
              FROM unnest(r.silver_managers) AS sm
              JOIN managers m ON m.id = sm
            ) AS silver_manager_names,
            ARRAY(
              SELECT m.name
              FROM unnest(r.bronze_managers) AS bm
              JOIN managers m ON m.id = bm
            ) AS bronze_manager_names
          FROM results r
          JOIN leagues l ON r.league_id = l.id
          JOIN competitions c ON l.competition_id = c.id
          JOIN sports s ON c.sport_id = s.id
        `;
    const values = [];
    const filters = [];

    if (leagueName) {
      filters.push("l.name ILIKE $" + (values.length + 1));
      values.push(`%${leagueName}%`);
    }
    if (sportId) {
      filters.push("s.id = $" + (values.length + 1));
      values.push(sportId);
    }
    if (competitionId) {
      filters.push("c.id = $" + (values.length + 1));
      values.push(competitionId);
    }
    if (tier) {
      filters.push("l.tier = $" + (values.length + 1));
      values.push(tier);
    }
    if (startYear) {
      filters.push("l.start_year = $" + (values.length + 1));
      values.push(startYear);
    }
    if (endYear) {
      filters.push("l.end_year = $" + (values.length + 1));
      values.push(endYear);
    }
    if (managerId) {
      filters.push(
        "$" +
          (values.length + 1) +
          " = ANY(r.gold_managers) OR $" +
          (values.length + 1) +
          " = ANY(r.silver_managers) OR $" +
          (values.length + 1) +
          " = ANY(r.bronze_managers)"
      );
      values.push(parseInt(managerId));
    }

    if (filters.length > 0) {
      query += " WHERE " + filters.join(" AND ");
    }

    const result = await pool.query(query, values);
    res.json(result.rows);
  } catch (error) {
    console.error("Error retrieving results:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/", async (req, res) => {
  const { leagueId, goldManagers, silverManagers, bronzeManagers } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO results (league_id, gold_managers, silver_managers, bronze_managers) VALUES ($1, $2, $3, $4) RETURNING *",
      [leagueId, goldManagers, silverManagers, bronzeManagers]
    );
    res.status(201).json(result.rows[0]);
  } catch (error) {
    console.error("Error adding result:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { leagueId, goldManagers, silverManagers, bronzeManagers } = req.body;
  try {
    const result = await pool.query(
      "UPDATE results SET league_id = $1, gold_managers = $2, silver_managers = $3, bronze_managers = $4 WHERE id = $5 RETURNING *",
      [leagueId, goldManagers, silverManagers, bronzeManagers, id]
    );
    if (result.rows.length === 0) {
      res.status(404).json({ error: "Result not found" });
    } else {
      res.json(result.rows[0]);
    }
  } catch (error) {
    console.error("Error updating result:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await pool.query("DELETE FROM results WHERE id = $1", [id]);
    res.sendStatus(204);
  } catch (error) {
    console.error("Error deleting result:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
