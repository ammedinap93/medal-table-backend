# Pull code
cd /var/www/html/medal_table_bk/
git checkout main
git pull origin main

# Build and deploy
yarn install
pm2 stop medal-table-backend
pm2 start app.js --name medal-table-backend
