require("dotenv").config();
const express = require("express");
const cors = require("cors");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const app = express();
const port = process.env.PORT || 5000;

const pool = require("./db");

const { authenticateToken } = require("./auth");
const managersRouter = require("./routes/managers");
const sportsRouter = require("./routes/sports");
const competitionsRouter = require("./routes/competitions");
const leaguesRouter = require("./routes/leagues");
const resultsRouter = require("./routes/results");
const resultsLogsRouter = require("./routes/resultsLogs");
const medalTableConfigsRouter = require("./routes/medalTableConfigs");
const medalTablesRouter = require("./routes/medalTables");
const individualAwardsRouter = require("./routes/individualAwards");
const individualAwardResultsRouter = require("./routes/individualAwardResults");
const individualAwardResultsLogsRouter = require("./routes/individualAwardResultsLogs");

app.use(cors());
app.use(express.json());

app.use("/managers", authenticateToken, managersRouter);
app.use("/sports", authenticateToken, sportsRouter);
app.use("/competitions", authenticateToken, competitionsRouter);
app.use("/leagues", authenticateToken, leaguesRouter);
app.use("/results", authenticateToken, resultsRouter);
app.use("/results-logs", resultsLogsRouter);
app.use("/medal-table-configs", authenticateToken, medalTableConfigsRouter);
app.use("/medal-tables", medalTablesRouter);
app.use("/individual-awards", authenticateToken, individualAwardsRouter);
app.use(
  "/individual-award-results",
  authenticateToken,
  individualAwardResultsRouter
);
app.use("/individual-award-results-logs", individualAwardResultsLogsRouter);

app.post("/login", async (req, res) => {
  const { username, password } = req.body;

  try {
    const result = await pool.query("SELECT * FROM users WHERE username = $1", [
      username,
    ]);
    const user = result.rows[0];

    if (!user) {
      return res.status(401).json({ error: "Invalid username or password" });
    }

    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      return res.status(401).json({ error: "Invalid username or password" });
    }

    const token = jwt.sign(
      { id: user.id, username: user.username, isAdmin: user.is_admin },
      process.env.JWT_SECRET
    );

    res.json({ token });
  } catch (error) {
    console.error("Error during login:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
